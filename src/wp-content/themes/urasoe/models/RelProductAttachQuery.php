<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[RelProductAttachModel]].
 *
 * @see RelProductAttachModel
 */
class RelProductAttachQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return RelProductAttachModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return RelProductAttachModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}