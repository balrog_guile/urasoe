<?php
/* =============================================================================
 * WPサインアップのためのバリデーション専用モデル
 ============================================================================ */
namespace app\models;
use Yii;
use yii\base\Model;
class WpUsersValidationModel extends Model
{
    public $user_login;
    //public $display_name;
    public $user_email;
    public $user_pass;
    
    public $valiationMode = 'input';
    // ----------------------------------------------------
    /**
     * コンストラクタ
     */
    public function __construct($config = array()) {
        parent::__construct($config);
    }
    
    // ----------------------------------------------------
    /**
     * ラベル
     */
    public function attributeLabels()
    {
        return [
            'user_login' => Yii::t('app', 'ログインID'),
            'user_email' => Yii::t('app', 'メールアドレス'),
            'user_pass' => Yii::t('app', 'パスワード'),
        ];
    }
    
    // ----------------------------------------------------
    /**
     * バリデーションルール
     */
    public function rules()
    {
        if( $this->getScenario() == 'register' )
        {
            return [
                [['user_login', 'user_pass'], 'required', 'on' => 'register'],
                [['user_email'], 'email', 'on' => 'register'],
                [[ 'user_email', 'valiationMode'], 'safe', 'on' => 'register'],
            ];
        }
        return [
            [['user_login', 'user_email', 'user_pass'], 'required'],
            [['user_email'], 'email'],
            [['valiationMode'], 'safe'],
        ];
    }
    
    // ----------------------------------------------------
}