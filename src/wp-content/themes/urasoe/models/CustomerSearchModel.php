<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerModel;

/**
 * CustomerSearchModel represents the model behind the search form about `app\models\CustomerModel`.
 */
class CustomerSearchModel extends CustomerModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'customer_status'], 'integer'],
            [['name1', 'name2', 'kana1', 'kana2', 'zip', 'pref', 'address1', 'address2', 'address3', 'company_name', 'company_kana', 'honor', 'campany_honor', 'tel1', 'tel2', 'tel3', 'fax1', 'fax2', 'mailaddress1', 'mailaddress2', 'url1', 'url2', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'customer_status' => $this->customer_status,
        ]);

        $query->andFilterWhere(['like', 'name1', $this->name1])
            ->andFilterWhere(['like', 'name2', $this->name2])
            ->andFilterWhere(['like', 'kana1', $this->kana1])
            ->andFilterWhere(['like', 'kana2', $this->kana2])
            ->andFilterWhere(['like', 'zip', $this->zip])
            ->andFilterWhere(['like', 'pref', $this->pref])
            ->andFilterWhere(['like', 'address1', $this->address1])
            ->andFilterWhere(['like', 'address2', $this->address2])
            ->andFilterWhere(['like', 'address3', $this->address3])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'company_kana', $this->company_kana])
            ->andFilterWhere(['like', 'honor', $this->honor])
            ->andFilterWhere(['like', 'campany_honor', $this->campany_honor])
            ->andFilterWhere(['like', 'tel1', $this->tel1])
            ->andFilterWhere(['like', 'tel2', $this->tel2])
            ->andFilterWhere(['like', 'tel3', $this->tel3])
            ->andFilterWhere(['like', 'fax1', $this->fax1])
            ->andFilterWhere(['like', 'fax2', $this->fax2])
            ->andFilterWhere(['like', 'mailaddress1', $this->mailaddress1])
            ->andFilterWhere(['like', 'mailaddress2', $this->mailaddress2])
            ->andFilterWhere(['like', 'url1', $this->url1])
            ->andFilterWhere(['like', 'url2', $this->url2]);

        return $dataProvider;
    }
}
