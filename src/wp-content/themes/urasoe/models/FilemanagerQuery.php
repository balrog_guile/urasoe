<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[FilemanagerModel]].
 *
 * @see FilemanagerModel
 */
class FilemanagerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return FilemanagerModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return FilemanagerModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}