<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerFieldDefineModel;

/**
 * CustomerFieldDefineSearchModel represents the model behind the search form about `app\models\CustomerFieldDefineModel`.
 */
class CustomerFieldDefineSearchModel extends CustomerFieldDefineModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rank'], 'integer'],
            [['data_name', 'label', 'data_type', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerFieldDefineModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'rank' => $this->rank,
        ]);

        $query->andFilterWhere(['like', 'data_name', $this->data_name])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'data_type', $this->data_type]);

        return $dataProvider;
    }
}
