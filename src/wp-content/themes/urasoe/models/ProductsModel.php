<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%products}}".
 *
 * @property integer $id
 * @property integer $wp_post_id
 * @property string $item_id
 * @property string $item_name
 * @property string $url
 * @property integer $open_status
 * @property string $meta_key
 * @property string $meta_desc
 * @property string $view_commente
 * @property string $detail_commente
 * @property string $create_at
 * @property string $update_at
 */
class ProductsModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wp_post_id', 'open_status'], 'integer'],
            [['item_id', 'item_name', 'url', 'meta_key', 'meta_desc', 'create_at', 'update_at'], 'required'],
            [['meta_desc', 'view_commente', 'detail_commente'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['item_id', 'item_name', 'url'], 'string', 'max' => 255],
            [['meta_key'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'wp_post_id' => Yii::t('app', 'WPのポストのID（投稿と紐づく場合）'),
            'item_id' => Yii::t('app', '品番'),
            'item_name' => Yii::t('app', '品名'),
            'url' => Yii::t('app', 'URLパス'),
            'open_status' => Yii::t('app', '公開状態'),
            'meta_key' => Yii::t('app', 'メタキーワード'),
            'meta_desc' => Yii::t('app', 'メタ詳細'),
            'view_commente' => Yii::t('app', '一覧画面説明'),
            'detail_commente' => Yii::t('app', '詳細説明'),
            'create_at' => Yii::t('app', '作成日時'),
            'update_at' => Yii::t('app', '更新日時'),
        ];
    }

    /**
     * @inheritdoc
     * @return ProductsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductsQuery(get_called_class());
    }
}
