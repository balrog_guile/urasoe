<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%product_add_text}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $title
 * @property string $content
 * @property string $create_at
 * @property string $update_at
 * @property integer $rank
 */
class ProductAddTextModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_add_text}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'title', 'content', 'create_at', 'update_at'], 'required'],
            [['product_id', 'rank'], 'integer'],
            [['content'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', '商品ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'create_at' => Yii::t('app', '作成日'),
            'update_at' => Yii::t('app', '更新日時'),
            'rank' => Yii::t('app', 'Rank'),
        ];
    }

    /**
     * @inheritdoc
     * @return ProductAddTextQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductAddTextQuery(get_called_class());
    }
}
