<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FilemanagerModel;

/**
 * FilemanagerSearchModel represents the model behind the search form about `app\models\FilemanagerModel`.
 */
class FilemanagerSearchModel extends FilemanagerModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'wp_attach_id'], 'integer'],
            [['wp_file_path', 'wp_url', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FilemanagerModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'wp_attach_id' => $this->wp_attach_id,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'wp_file_path', $this->wp_file_path])
            ->andFilterWhere(['like', 'wp_url', $this->wp_url]);

        return $dataProvider;
    }
}
