<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%customer}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name1
 * @property string $name2
 * @property string $kana1
 * @property string $kana2
 * @property string $zip
 * @property string $pref
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $company_name
 * @property string $company_kana
 * @property string $honor
 * @property string $campany_honor
 * @property string $tel1
 * @property string $tel2
 * @property string $tel3
 * @property string $fax1
 * @property string $fax2
 * @property string $mailaddress1
 * @property string $mailaddress2
 * @property string $url1
 * @property string $url2
 * @property string $create_at
 * @property string $update_at
 * @property integer $customer_status
 */
class CustomerModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name1', 'name2', 'kana1', 'kana2', 'zip', 'pref', 'address1', 'address2', 'create_at', 'update_at', 'tel1'], 'required'],
            [['user_id', 'customer_status'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['name1', 'name2', 'kana1', 'kana2', 'address1', 'address2', 'address3', 'company_name', 'company_kana', 'mailaddress1', 'mailaddress2', 'url1', 'url2'], 'string', 'max' => 255],
            [['zip'], 'string', 'max' => 8],
            [['pref'], 'string', 'max' => 12],
            [['honor', 'campany_honor'], 'string', 'max' => 45],
            [['tel2', 'tel3', 'fax1', 'fax2'], 'string', 'max' => 55]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name1' => Yii::t('app', '名前（姓）'),
            'name2' => Yii::t('app', '名前（名）'),
            'kana1' => Yii::t('app', 'かな（姓）'),
            'kana2' => Yii::t('app', 'かな（名）'),
            'zip' => Yii::t('app', '郵便番号'),
            'pref' => Yii::t('app', '都道府県'),
            'address1' => Yii::t('app', '市区町村'),
            'address2' => Yii::t('app', '町域・番地'),
            'address3' => Yii::t('app', 'マンション名など'),
            'company_name' => Yii::t('app', '会社名など'),
            'company_kana' => Yii::t('app', '会社名など、かな'),
            'honor' => Yii::t('app', '敬称'),
            'campany_honor' => Yii::t('app', '会社名など敬称'),
            'tel1' => Yii::t('app', '電話番号'),
            'tel2' => Yii::t('app', '電話番号2'),
            'tel3' => Yii::t('app', '電話番号3'),
            'fax1' => Yii::t('app', 'FAX1'),
            'fax2' => Yii::t('app', 'FAX2'),
            'mailaddress1' => Yii::t('app', 'メールアドレス1'),
            'mailaddress2' => Yii::t('app', 'メールアドレス2'),
            'url1' => Yii::t('app', 'URL1'),
            'url2' => Yii::t('app', 'URL2'),
            'create_at' => Yii::t('app', '作成日'),
            'update_at' => Yii::t('app', '更新日'),
            'customer_status' => Yii::t('app', '顧客ステータス'),
        ];
    }

    /**
     * @inheritdoc
     * @return CustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerQuery(get_called_class());
    }
}
