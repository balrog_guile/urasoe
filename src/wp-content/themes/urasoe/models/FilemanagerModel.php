<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%filemanager}}".
 *
 * @property integer $id
 * @property string $wp_file_path
 * @property string $wp_url
 * @property integer $wp_attach_id
 * @property string $create_at
 * @property string $update_at
 */
class FilemanagerModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%filemanager}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wp_file_path', 'wp_url', 'wp_attach_id', 'create_at', 'update_at'], 'required'],
            [['wp_attach_id'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['wp_file_path', 'wp_url'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'wp_file_path' => Yii::t('app', 'WPから取得したパス'),
            'wp_url' => Yii::t('app', 'wpから取得したURL'),
            'wp_attach_id' => Yii::t('app', 'WordPressの添付ID'),
            'create_at' => Yii::t('app', '作成日'),
            'update_at' => Yii::t('app', '更新日'),
        ];
    }

    /**
     * @inheritdoc
     * @return FilemanagerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FilemanagerQuery(get_called_class());
    }
}
