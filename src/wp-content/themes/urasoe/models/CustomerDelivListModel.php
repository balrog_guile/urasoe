<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%customer_deliv_list}}".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $name1
 * @property string $name2
 * @property string $kana1
 * @property string $kana2
 * @property string $zip
 * @property string $pref
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $company_kana
 * @property string $company_name
 * @property string $tel
 * @property string $update_at
 * @property string $create_at
 * @property integer $rank
 */
class CustomerDelivListModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_deliv_list}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'name1', 'name2', 'kana1', 'kana2', 'zip', 'pref', 'address1', 'address2', 'update_at', 'create_at', 'tel'], 'required'],
            [['customer_id', 'rank'], 'integer'],
            [['update_at', 'create_at'], 'safe'],
            [['name1', 'name2', 'kana1', 'kana2', 'tel'], 'string', 'max' => 55],
            [['zip'], 'string', 'max' => 25],
            [['pref'], 'string', 'max' => 45],
            [['address1', 'address2', 'address3', 'company_kana', 'company_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'customer_id' => Yii::t('app', 'カスタマーID'),
            'name1' => Yii::t('app', '姓'),
            'name2' => Yii::t('app', '名'),
            'kana1' => Yii::t('app', 'かな（姓）'),
            'kana2' => Yii::t('app', 'Kana2'),
            'zip' => Yii::t('app', '郵便番号'),
            'pref' => Yii::t('app', '都道府県'),
            'address1' => Yii::t('app', '市町村区'),
            'address2' => Yii::t('app', '町域番地'),
            'address3' => Yii::t('app', 'マンションなど'),
            'company_kana' => Yii::t('app', '会社名など、かな'),
            'company_name' => Yii::t('app', '会社名など'),
            'tel' => Yii::t('app', '電話番号'),
            'update_at' => Yii::t('app', '更新日'),
            'create_at' => Yii::t('app', '作成日'),
            'rank' => Yii::t('app', 'Rank'),
        ];
    }

    /**
     * @inheritdoc
     * @return CustomerDelivListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerDelivListQuery(get_called_class());
    }
}
