<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%rel_product_attach}}".
 *
 * @property integer $product_id
 * @property integer $attache_id
 * @property integer $is_main
 * @property integer $is_thumb
 * @property integer $rank
 * @property string $label
 * @property string $title
 * @property string $comment
 * @property string $create_at
 * @property string $update_at
 */
class RelProductAttachModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rel_product_attach}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'attache_id', 'create_at', 'update_at'], 'required'],
            [['product_id', 'attache_id', 'is_main', 'is_thumb', 'rank'], 'integer'],
            [['label', 'title', 'comment'], 'string'],
            [['create_at', 'update_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('app', 'Product ID'),
            'attache_id' => Yii::t('app', 'Attache ID'),
            'is_main' => Yii::t('app', 'メイン画像'),
            'is_thumb' => Yii::t('app', 'サムネイル指定'),
            'rank' => Yii::t('app', '表示順'),
            'label' => Yii::t('app', 'ラベル（alt)'),
            'title' => Yii::t('app', 'title属性'),
            'comment' => Yii::t('app', 'コメント'),
            'create_at' => Yii::t('app', '作成日時'),
            'update_at' => Yii::t('app', '更新日時'),
        ];
    }

    /**
     * @inheritdoc
     * @return RelProductAttachQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RelProductAttachQuery(get_called_class());
    }
}
