<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductsModel;

/**
 * ProductsSearchModel represents the model behind the search form about `app\models\ProductsModel`.
 */
class ProductsSearchModel extends ProductsModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'wp_post_id', 'open_status'], 'integer'],
            [['item_id', 'item_name', 'url', 'meta_key', 'meta_desc', 'view_commente', 'detail_commente', 'create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'wp_post_id' => $this->wp_post_id,
            'open_status' => $this->open_status,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'item_id', $this->item_id])
            ->andFilterWhere(['like', 'item_name', $this->item_name])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'meta_key', $this->meta_key])
            ->andFilterWhere(['like', 'meta_desc', $this->meta_desc])
            ->andFilterWhere(['like', 'view_commente', $this->view_commente])
            ->andFilterWhere(['like', 'detail_commente', $this->detail_commente]);

        return $dataProvider;
    }
}
