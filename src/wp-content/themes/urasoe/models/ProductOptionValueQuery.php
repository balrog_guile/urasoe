<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ProductOptionValueModel]].
 *
 * @see ProductOptionValueModel
 */
class ProductOptionValueQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ProductOptionValueModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProductOptionValueModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}