<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%customer_field_define}}".
 *
 * @property integer $id
 * @property string $data_name
 * @property string $label
 * @property string $data_type
 * @property string $create_at
 * @property string $update_at
 * @property integer $rank
 */
class CustomerFieldDefineModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_field_define}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_name', 'label', 'data_type', 'create_at', 'update_at'], 'required'],
            [['create_at', 'update_at'], 'safe'],
            [['rank'], 'integer'],
            [['data_name', 'label', 'data_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data_name' => Yii::t('app', 'データ名'),
            'label' => Yii::t('app', 'データ名ラベル'),
            'data_type' => Yii::t('app', 'データ・タイプの定義文字列'),
            'create_at' => Yii::t('app', '作成日'),
            'update_at' => Yii::t('app', '更新日'),
            'rank' => Yii::t('app', '表示順'),
        ];
    }

    /**
     * @inheritdoc
     * @return CustomerFieldDefineQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerFieldDefineQuery(get_called_class());
    }
}
