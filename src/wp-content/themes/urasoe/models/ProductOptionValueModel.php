<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%product_option_value}}".
 *
 * @property integer $id
 * @property integer $product_option_id
 * @property string $label
 * @property string $value
 * @property integer $rank
 * @property string $create_at
 * @property string $update_at
 */
class ProductOptionValueModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_option_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_option_id', 'label', 'value', 'rank', 'create_at', 'update_at'], 'required'],
            [['product_option_id', 'rank'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['label', 'value'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_option_id' => Yii::t('app', 'Product Option ID'),
            'label' => Yii::t('app', '表示ラベル'),
            'value' => Yii::t('app', 'Value'),
            'rank' => Yii::t('app', 'Rank'),
            'create_at' => Yii::t('app', '作成日'),
            'update_at' => Yii::t('app', 'Update At'),
        ];
    }

    /**
     * @inheritdoc
     * @return ProductOptionValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductOptionValueQuery(get_called_class());
    }
}
