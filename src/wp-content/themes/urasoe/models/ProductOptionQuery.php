<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ProductOptionModel]].
 *
 * @see ProductOptionModel
 */
class ProductOptionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ProductOptionModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProductOptionModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}