<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%product_sku}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $sku_id
 * @property string $sku_name
 * @property integer $price
 * @property integer $sale_price
 * @property integer $campaign_price
 * @property string $campaign_begin
 * @property string $campaign_end
 * @property string $comment
 * @property string $create_at
 * @property string $update_at
 * @property integer $rank
 */
class ProductSkuModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_sku}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'sku_id', 'sku_name', 'create_at', 'update_at'], 'required'],
            [['product_id', 'price', 'sale_price', 'campaign_price', 'rank'], 'integer'],
            [['campaign_begin', 'campaign_end', 'create_at', 'update_at'], 'safe'],
            [['comment'], 'string'],
            [['sku_id', 'sku_name'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'sku_id' => Yii::t('app', '枝番'),
            'sku_name' => Yii::t('app', 'sku名'),
            'price' => Yii::t('app', '定価'),
            'sale_price' => Yii::t('app', '売価'),
            'campaign_price' => Yii::t('app', 'キャンペーン価格'),
            'campaign_begin' => Yii::t('app', 'キャンペーンスタート日時'),
            'campaign_end' => Yii::t('app', 'キャンペーンスタート日時'),
            'comment' => Yii::t('app', 'Comment'),
            'create_at' => Yii::t('app', '作成日時'),
            'update_at' => Yii::t('app', '更新日時'),
            'rank' => Yii::t('app', '表示順'),
        ];
    }

    /**
     * @inheritdoc
     * @return ProductSkuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductSkuQuery(get_called_class());
    }
}
