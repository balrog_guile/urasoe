<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%rel_sku_attach}}".
 *
 * @property integer $sku_id
 * @property integer $attache_id
 * @property integer $rank
 * @property string $title
 * @property string $comment
 * @property string $label
 * @property string $update_at
 * @property string $create_at
 */
class RelSkuAttachModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rel_sku_attach}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sku_id', 'attache_id', 'update_at', 'create_at'], 'required'],
            [['sku_id', 'attache_id', 'rank'], 'integer'],
            [['title', 'comment', 'label'], 'string'],
            [['update_at', 'create_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sku_id' => Yii::t('app', 'Sku ID'),
            'attache_id' => Yii::t('app', 'Attache ID'),
            'rank' => Yii::t('app', '表示順'),
            'title' => Yii::t('app', 'title属性'),
            'comment' => Yii::t('app', 'コメント'),
            'label' => Yii::t('app', 'ラベル（alt)'),
            'update_at' => Yii::t('app', '更新日時'),
            'create_at' => Yii::t('app', '作成日時'),
        ];
    }

    /**
     * @inheritdoc
     * @return RelSkuAttachQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RelSkuAttachQuery(get_called_class());
    }
}
