<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ProductAddTextModel]].
 *
 * @see ProductAddTextModel
 */
class ProductAddTextQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ProductAddTextModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProductAddTextModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}