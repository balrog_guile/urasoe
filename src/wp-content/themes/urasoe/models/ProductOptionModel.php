<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%product_option}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $code
 * @property string $name
 * @property integer $data_type
 * @property integer $required
 * @property integer $rank
 * @property string $create_at
 * @property string $update_at
 * @property integer $open_status
 * @property string $initial
 */
class ProductOptionModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'create_at', 'update_at'], 'required'],
            [['product_id', 'data_type', 'required', 'rank', 'open_status'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['initial'], 'string'],
            [['code', 'name'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', '商品ID'),
            'code' => Yii::t('app', 'オプションコード'),
            'name' => Yii::t('app', 'オプション名'),
            'data_type' => Yii::t('app', 'データ・タイプ'),
            'required' => Yii::t('app', '必須'),
            'rank' => Yii::t('app', '表示順'),
            'create_at' => Yii::t('app', '作成日'),
            'update_at' => Yii::t('app', '更新日'),
            'open_status' => Yii::t('app', '公開状態'),
            'initial' => Yii::t('app', '初期値'),
        ];
    }

    /**
     * @inheritdoc
     * @return ProductOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductOptionQuery(get_called_class());
    }
}
