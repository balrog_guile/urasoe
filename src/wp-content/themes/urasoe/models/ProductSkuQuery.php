<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ProductSkuModel]].
 *
 * @see ProductSkuModel
 */
class ProductSkuQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ProductSkuModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProductSkuModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}