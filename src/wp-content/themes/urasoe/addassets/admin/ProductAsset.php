<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\addassets\admin;
use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ProductAsset extends AssetBundle
{
    //public $basePath = '@webroot/addassets/admin';
    //public $baseUrl;
    public function init() {
        parent::init();
        $this->basePath = get_template_directory() . '/addassets/admin/';
        $this->baseUrl =  get_template_directory_uri() . '/addassets/admin/';
    }
    public $css = [
        'product/css/form.css'
    ];
    public $js = [
        'product/js/drag_and_drop.js',
        'js/fileupload.js',
        'js/knockout.js',
        'product/js/form.js',
    ];
    public $depends = [
        'app\addassets\admin\AdminAsset'
    ];
}
/*
 * FileAPIライブラリ
 * http://rubaxa.github.io/jquery.fileapi/
 */