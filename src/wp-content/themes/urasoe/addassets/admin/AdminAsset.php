<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\addassets\admin;
use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    //public $basePath = '@webroot/addassets/admin';
    //public $baseUrl;
    public function init() {
        parent::init();
        $this->basePath = get_template_directory() . '/addassets/admin/';
        $this->baseUrl =  get_template_directory_uri() . '/addassets/admin/';
    }
    public $css = [
        'css/common.css'
    ];
    public $js = [
        'js/jquery.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
