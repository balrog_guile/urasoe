/* =============================================================================
 * ファイルアップロード
 * ========================================================================== */
function FileUpload( param ){
    
    var self = this;
    this.append = param;
    
    // ----------------------------------------------------
    /**
     * ファイルリストから処理
     */
    this.upload = function( files, url, name, callback, errorCallback ){
        
        var _form = $('<form id="send"></form>');
        var formData = new FormData(_form.get(0));
        var i = 0;
        while( i < files.length )
        {
            formData.append( name + '[]', files[i]);
            i ++ ;
        }
        
        //YiiのCSRF
        var csrfName = $('meta[name="csrf-param"]').attr('content');
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        formData.append( csrfName, csrfToken );
        
        
        // 処理実行
        $.ajax(
            url,
            {
                'type': 'post',
                'contentType': false,
                'processData': false,
                'data': formData,
                'dataType': 'json',
                'success': function( data, dataType ){
                    callback( data, dataType, self );
                },
                'error': errorCallback
            }
        );
        
    };
    
    // ----------------------------------------------------
    
}