/* =============================================================================
 * 商品編集フォーム
 * ========================================================================== */
$(document).ready(function(){
    
    
    // ----------------------------------------------------
    /**
     * knockout用画像リストオブジェクト
     */
    var koImage = function(){
        
        this.imageUrl = ko.observable();
        this.attache_id = ko.observable();
        this.label = ko.observable();
        this.title = ko.observable();
        this.comment = ko.observable();
        
        // ------------------------------------------------
        /**
         * 順番変更
         * @returns {form_L4.koClass}
         */
        this.rankChange = function( mode )
        {
            //alert( viewModel.productImage.indexOf( this ) );
            //処理無視
            var index =  viewModel.productImage.indexOf( this );
            var len = viewModel.productImage().length;
            if( mode == 'up' && index == 0 )
            {
                return;
            }
            
            if(
                ( mode == 'down' ) &&
                ( ( len - 1 ) == index )
            ){
                return;
            }
            
            //変更実行
            if( mode == 'up' )
            {
                var clone = viewModel.productImage.splice( index, 1 );
                var setIndex = index - 1;
                var reset = [];
                var i = 0;
                var len = viewModel.productImage().length;
                while( i < len )
                {
                    if( i == setIndex )
                    {
                        reset.push( clone[0] );
                    }
                    var set = viewModel.productImage()[i];
                    reset.push( set );
                    i ++ ;
                }
            }
            else
            {
                var clone = viewModel.productImage.splice( index, 1 );
                var setIndex = index ;
                var reset = [];
                var i = 0;
                var len = viewModel.productImage().length;
                while( i < len )
                {
                    var set = viewModel.productImage()[i];
                    reset.push( set );
                    if( i == setIndex )
                    {
                        reset.push( clone[0] );
                    }
                    i ++ ;
                }
            }
            //alert( reset.length );
            var i = 0;
            viewModel.productImage.removeAll();
            while( i < reset.length )
            {
                viewModel.productImage.push( reset[i] );
                i ++ ;
            }
            //viewModel.productImage = reset;
        }
        
        // ------------------------------------------------
    }
    
    
    
    
    // ----------------------------------------------------
    /**
     * オプション用
     */
    var productOption = function(param){
        
        var self = this;
        
        //入力値
        this.values = ko.observableArray();
        
        //スキーム定義
        for( var key in tableSchema.productOption )
        {
            this[tableSchema.productOption[key]] = ko.observable();
        }
        
        
        // ------------------------------------------------
        /**
         * オプション値追加
         */
        this.addOptionValue = function( data, event, elem ){
            addOptionValueBtn( data, event, elem, self );
        }
        // ------------------------------------------------
        /**
         * 順番変更
         */
        this.rankChange = function( data, event, elem )
        {
            //rankChange = function( self, target, mode )
            var mode = $(elem).attr('data-mode');
            rankChange( self, viewModel.productOption, mode );
            
        }
        // ------------------------------------------------
        /**
         * 削除処理
         */
        this.lineDelete = function( data, event, elem ){
            koArrayDelete(  self, viewModel.productOption );
        }
        
        // ----------------------------------------------------
    }
    
    
    
    // ----------------------------------------------------
    /**
     * オプション値用
     */
    var productOptionValue = function(param){
        
    };
    
    
    
    // ----------------------------------------------------
    /**
     * SKU用クラス
     */
    var skuList = function(){
        
        var self = this;
        this.skuImage = ko.observableArray();
        
        // ------------------------------------------------
        /**
         * カウント
         */
        this.count = ko.computed(function() {
            return viewModel.skuList.indexOf(this);
        }, this);
        this.countDisplay = ko.computed(function() {
            return viewModel.skuList.indexOf(this)+1;
        }, this);
        
        
        // ------------------------------------------------
        /**
         * SKU用イメージクラス
         * @returns {form_L4.skuList}
         */
        var skuImage = function(){
            this.imageUrl = ko.observable();
            this.attache_id = ko.observable();
            this.label = ko.observable();
            this.title = ko.observable();
            this.comment = ko.observable();
            
            
        };
        
        // ------------------------------------------------
        /**
         * ファイルアップロード完了時
         */
        var skuUploadError = function(eve){
            alert('通信エラー');
            return;
        }
        var skuUploaSuccess = function( data, dataType, param ){
            
             //エラー
            if( data.errorCount > 0 )
            {
                alert('アップロードエラーが発生した写真があります');
                var i = 0;
                var len = data.errorResult.length;
                var message = [];
                while( i < len ){
                    message.push( data.errorResult[i].error );
                    i ++ ;
                }
                alert( message );
            }
            
            
            //追加
            var i = 0;
            var len = data.okResult.length;
            while( i < len )
            {
                var addImage = new skuImage();
                addImage.imageUrl(data.okResult[i].url);
                addImage.attache_id( data.okResult[i].attach_id );
                self.skuImage.push(addImage);
                i ++ ;
            }
        }
        
        
        // ------------------------------------------------
        /**
         * 順番変更
         */
        this.rankChange = function( data, event, elem )
        {
            //rankChange = function( self, target, mode )
            var mode = $(elem).attr('data-mode');
            rankChange( self, viewModel.skuList, mode );
            
        }
        // ------------------------------------------------
        /**
         * 画像選択アップロード
         */
        this.fileChange = function(data, event, elem ){
            //console.log(elem.files);//ファイルの撮り方
        };
        
        
        
        // ------------------------------------------------
        /**
         * ドラッグ
         */
        this.dragEnter = function( data, event, element ){
            event.preventDefault();
            $(element).addClass('on-image-list-droparea');
        };
        this.dragover = function( data, event, element ){
            event.preventDefault();
            $(element).addClass('on-image-list-droparea');
        };
        this.dragLeave = function( data, event, element ){
            event.preventDefault();
            $(element).removeClass('on-image-list-droparea');
        };
        this.drop = function( data, event, element ){
            event.preventDefault();
            var files = event.originalEvent.dataTransfer.files;
            //アップローダー
            var upload = new FileUpload({});
            upload.upload(
                files,
                '/manage/filemanager/api',
                'files',
                skuUploaSuccess,
                skuUploadError
            );
            $(element).removeClass('on-image-list-droparea');
        }
        
        
        
        
        // ------------------------------------------------
    };
    
    
    
    // ----------------------------------------------------
    /**
     * 追加テキスト用クラス
     */
    var addText = function( param ){
        var self = this;
        
    };
    
    
    // ----------------------------------------------------
    /**
     * knockoutデータアサイン
     */
    var koClass = function(){
        //画像リスト用
        this.productImage = ko.observableArray();
        //SKU用
        this.skuList = ko.observableArray();
        //オプション用
        this.productOption = ko.observableArray();
        //追加テキスト
        this.addText = ko.observableArray(); 
        
    };
    var viewModel = new koClass();
    ko.applyBindings(viewModel);
    
    
    
    
    // ----------------------------------------------------
    /**
     * 画像追加時のコールバック
     */
    var fileUploadError = function(e){
        alert('通信エラー');
    };
    
    //ファイルアップロード完了時
    var fileUploadSuccess = function( data ){
        
        //エラー
        if( data.errorCount > 0 )
        {
            alert('アップロードエラーが発生した写真があります');
            var i = 0;
            var len = data.errorResult.length;
            var message = [];
            while( i < len ){
                message.push( data.errorResult[i].error );
                i ++ ;
            }
            alert( message );
        }
        
        //追加
        var i = 0;
        var len = data.okResult.length;
        while( i < len )
        {
            var addImage = new koImage();
            addImage.imageUrl(data.okResult[i].url);
            addImage.attache_id( data.okResult[i].attach_id );
            viewModel.productImage.push(addImage);
            i ++ ;
        }
    };
    
    
    
    
    // ----------------------------------------------------
    /**
     * 追加画像
     */
    var imageClone = $('#image-list li').eq(0).clone(true);
    var imageDrop = function( self, files ){
        //アップローダー
        var upload = new FileUpload();
        upload.upload(
            files,
            '/manage/filemanager/api',
            'files',
            fileUploadSuccess,
            fileUploadError
        );
    };
    $('#image-list li').eq(0).remove();
    var dandd1 = new urasoeDragAndDrop({
        'selector': '#image-list-droparea',
        'hoverClass': 'on-image-list-droparea',
        'drop': imageDrop
    });
    
    
    
    
    // ----------------------------------------------------
    /*
     * 画像追加（選択ボタン)
     */
    $('.image-add-btn').on(
        'change',
        function(evnt){
            //アップローダー
            var upload = new FileUpload();
            upload.upload(
                $(this).get(0).files,
                '/manage/filemanager/api',
                'files',
                fileUploadSuccess,
                fileUploadError
            );
            
        }
    );
    
    
    
    
    // ----------------------------------------------------
    /**
     * SKUの追加
     */
    $('.btn-add-sku').on(
        'click',
        function(e){
            var newSku = new skuList();
            viewModel.skuList.push( newSku );
        }
    );
    
    
    
    // ----------------------------------------------------
    /**
     * 画像追加時のコールバック
     */
    var skuFileUploadError = function(e){
        alert('通信エラー');
    };
    
    //ファイルアップロード完了時
    var skuFileUploadSuccess = function( data ){
        
        //エラー
        if( data.errorCount > 0 )
        {
            alert('アップロードエラーが発生した写真があります');
            var i = 0;
            var len = data.errorResult.length;
            var message = [];
            while( i < len ){
                message.push( data.errorResult[i].error );
                i ++ ;
            }
            alert( message );
        }
        
        //追加
        var i = 0;
        var len = data.okResult.length;
        while( i < len )
        {
            var addImage = new koImage();
            addImage.imageUrl(data.okResult[i].url);
            addImage.attache_id( data.okResult[i].attach_id );
            viewModel.productImage.push(addImage);
            i ++ ;
        }
    };
    
    // ----------------------------------------------------
    /**
     * オプション追加
     */
    $('.btn-add-option').on(
        'click',
        function(eve){
            var setObject = new productOption();
            viewModel.productOption.push( setObject );
        }
    );
    
    // ----------------------------------------------------
    /**
     * オプション値追加
     */
    var addOptionValueBtn = function( data, event, elem, self ){
        var setObject = new productOptionValue();
        self.values.push( setObject );
    };
    // ----------------------------------------------------
    /**
     * 追加テキスト追加
     */
    $('.btn-add-text').on(
        'click',
        function(e){
            var setObject = new addText;
            viewModel.addText.push( setObject );
        }
    );
    
    
    // ----------------------------------------------------
    /**
     * 順番変更メソッド
     */
    var rankChange = function( self, target, mode )
    {
        var index = target.indexOf( self );
        var len = target().length;
        
        //読み飛ばし
        if( index == 0 && mode == 'up' )
        {
            return;
        }
        if( ( index == (len-1) )&&( mode == 'down' ) )
        {
            return;
        }
        
        //設定しなおし
        var set = [];
        var targetObject = target.splice( index, 1 );
        var i = 0;
        var len = target().length;
        if( mode == 'up' )
        {
            var setIndex = index - 1;
        }
        else
        {
            var setIndex = index;
        }
        while( i < len )
        {
            if(( i == setIndex )&&(mode=='up') )
            {
                set.push( targetObject[0] );
            }
            set.push( target()[i] );
            if(( i == setIndex )&&(mode=='down') )
            {
                set.push( targetObject[0] );
            }
            i ++;
        }
        
        target.removeAll();
        var i = 0;
        var len = set.length;
        while( i < len )
        {
            target.push( set[i] );
            i ++ ;
        }
        
    }
    
    // ----------------------------------------------------
    /**
     * 削除
     */
    var koArrayDelete = function( self, target ){
        var index = target.indexOf( self );
        if( confirm('削除しますか?') )
        {
            var splice = target.splice( index, 1 );
        }
    }
    
    
    
    // ----------------------------------------------------
});