/* =============================================================================
 * ドラッグアンドドロップ
 * ========================================================================== */
function urasoeDragAndDrop( params ){
    
    //セルフ
    var self = this;
    
    //ターゲットにするセレクタ
    this.targetSelector = params['selector'];
    
    //ホバー時の追加クラス
    this.hoverClass = params.hoverClass||null;
    
    //ドロップ後
    this.setDrop = params.drop;
    
    
    
    // ----------------------------------------------------
    /**
     * dragenter
     */
    this.dragenter = function(e){
        e.preventDefault();
        
        if( self.hoverClass != null )
        {
            
            $( self.targetSelector ).addClass( self.hoverClass );
        }
    };
    
    // ----------------------------------------------------
    /**
     * dragover
     */
    this.dragover  = function(e){
        e.preventDefault();
        if( self.hoverClass != null )
        {
            $( self.targetSelector ).addClass( self.hoverClass );
        }
    };
    
    // ----------------------------------------------------
    /**
     * dragleave
     */
    this.dragleave = function(e){
        e.preventDefault();
        if( self.hoverClass != null )
        {
            $( self.targetSelector ).removeClass( self.hoverClass );
        }
    };
    // ----------------------------------------------------
    /**
     * drop
     */
    this.drop = function(e){
        e.preventDefault();
        
        //ファイルオブジェクト
        var files = e.originalEvent.dataTransfer.files;
        
        self.setDrop( self, files );
        
        if( self.hoverClass != null )
        {
            $( self.targetSelector ).removeClass( self.hoverClass );
        }
    }
    
    // ----------------------------------------------------
    /**
     * ドラッグ
     */
    $( this.targetSelector )
        .on('dragenter', this.dragenter )
        .on('dragover', this.dragover)
        .on( 'dragleave', this.dragleave )
        .on('drop', this.drop);
    
    // ----------------------------------------------------
    
};