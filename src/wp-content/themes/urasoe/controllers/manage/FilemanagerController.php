<?php
/* =============================================================================
 * ファイルマネージャー
 * ========================================================================== */
namespace app\controllers\manage;

use Yii;
use app\models\FilemanagerModel;
use app\models\FilemanagerSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\ErrorException;

class FilemanagerController extends Controller
{
    // ----------------------------------------------------
    /**
     * 振る舞い設定
     * @return type
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
        
    }
    
    // ----------------------------------------------------
    /**
     * アップデータ
     */
    public function actionApi()
    {
        //CSRFを無視
        Yii::$app->request->enableCsrfValidation = false;
        
        //ファイルデータがなければ404
        if(!isset($_FILES['files']) )
        {
            throw new NotFoundHttpException();
            return;
        }
        
        
        $return = [
            'errorCount' => 0,
            'okCount' => 0,
            'errorResult' => [],
            'okResult' => [],
        ];
        
        
        
        
        //ファイルリストを処理
        $i = 0;
        $len = count($_FILES['files']['name']);
        while( $i < $len )
        {
            $FILE = [
                'error' => $_FILES['files']['name'][$i],
                'type' => $_FILES['files']['type'][$i],
                'tmp_name' => $_FILES['files']['tmp_name'][$i],
                'size' => $_FILES['files']['size'][$i],
                'name' => $_FILES['files']['name'][$i],
            ];
            
            //アップロードエラー
            if(isset($FILE['error']) && $FILE['error'] > 0) {
                $return['errorCount'] ++;
                $return['errorResult'][] = [
                    'file' => null,
                    'url' => null,
                    'error' => 'PHPアップロードエラー'
                ];
                $i ++;
                continue;
            }
            
            //ファイル不正
            if( ! is_uploaded_file( $FILE['tmp_name'] ) )
            {
                $return['errorCount'] ++;
                $return['errorResult'][] = [
                    'file' => null,
                    'url' => null,
                    'error' => '不正アップロード'
                ];
                $i ++;
                continue;
            }
            
            
            ////WPにアップロード処理
            $uploaded_file = wp_upload_bits(
                    $FILE['name'],
                    null,
                    file_get_contents( $FILE['tmp_name'] )
                );
            
            
            //エラー処理
            if( $uploaded_file['error'] !== FALSE )
            {
                $return['errorCount'] ++;
                $return['errorResult'][] = $uploaded_file;
                $i ++;
                continue;
            }
            
            
            //添付処理
            $wp_filetype = wp_check_filetype( $uploaded_file['file'], null );
            $attachment = [
                'guid'           => $uploaded_file['file'], 
                'post_mime_type' => $wp_filetype['type'],
                'post_title'     => basename($uploaded_file['file']),
                'post_content'   => '',
                'post_status'    => 'inherit'
            ];
            $attach_id = wp_insert_attachment( $attachment, $uploaded_file['file'], 0 );
            
            $model = new FilemanagerModel();
            $model->wp_file_path = $uploaded_file['file'];
            $model->wp_url = $uploaded_file['url'];
            $model->wp_attach_id = $attach_id;
            $model->create_at = date('Y-m-d H:i:s');
            $model->update_at = date('Y-m-d H:i:s');
            $model->save();
            
            
            $i ++;
            $return['okCount'] ++;
            $uploaded_file['attach_id'] = $attach_id;
            $return['okResult'][] = $uploaded_file;
        }
        
        return json_encode( $return );
    }
    // ----------------------------------------------------
    /**
     * 一覧
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FilemanagerSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Displays a single FilemanagerModel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    // ----------------------------------------------------
    /**
     * 作成
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FilemanagerModel();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    // ----------------------------------------------------
    /**
     * アップデート
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    // ----------------------------------------------------
    /**
     * Deletes an existing FilemanagerModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    // ----------------------------------------------------
    /**
     * Finds the FilemanagerModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FilemanagerModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FilemanagerModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    // ----------------------------------------------------
}
