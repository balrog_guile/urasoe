<?php
/* =============================================================================
 * 製品管理
 * ========================================================================== */
namespace app\controllers\manage;

use Yii;
use app\models\ProductsModel;
use app\models\ProductsSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\RelProductAttachModel;
use app\models\ProductSkuModel;
use app\models\RelSkuAttachModel;
use app\models\ProductOptionModel;
use app\models\ProductOptionValueModel;
use app\models\ProductAddTextModel;

class ProductController extends Controller
{
    
    // ----------------------------------------------------
    /**
     * 振る舞い指定
     * @return type
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    // ----------------------------------------------------
    /**
     * 管理TOP
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    // ----------------------------------------------------
    /**
     * WP処理
     */
    protected function wppost( $model )
    {
        $post = [
            //'ID'             => [ <投稿 ID> ] // 既存の投稿を更新する場合に指定。
            'post_content'   => $model->detail_commente,//コンテンツ
            'post_name'      => $model->url,//投稿のスラッグ。
            'post_title'     => $model->item_name,// 投稿のタイトル。
            //'post_status'    => [ 'draft' | 'publish' | 'pending'| 'future' | 'private' | 登録済みカスタムステータス ] // 公開ステータス。デフォルトは 'draft'。
            'post_type'      => 'post',// 投稿タイプ。デフォルトは 'post'。
            //'post_author'    => [ <ユーザー ID> ] // 作成者のユーザー ID。デフォルトはログイン中のユーザーの ID。
            //'ping_status'    => [ 'closed' | 'open' ] // 'open' ならピンバック・トラックバックを許可。デフォルトはオプション 'default_ping_status' の値。
            //'post_parent'    => [ <投稿 ID> ] // 親投稿の ID。デフォルトは 0。
            //'menu_order'     => [ <順序値> ] // 固定ページを追加する場合、メニュー内の並び順を指定。デフォルトは 0。
            //'to_ping'        => // スペースまたは改行で区切った、ピンを打つ予定の URL のリスト。デフォルトは空文字列。
            //'pinged'         => // スペースまたは改行で区切った、ピンを打った URL のリスト。デフォルトは空文字列。
            //'post_password'  => [ <文字列> ] // 投稿パスワード。デフォルトは空文字列。
            'post_excerpt'   => $model->view_commente,// 投稿の抜粋。
            'post_date'      => $model->create_at,// 投稿の作成日時。
            'post_date_gmt'  => gmdate( 'Y-m-d H:i:s', strtotime($model->create_at) ),// 投稿の作成日時（GMT）。
            //'comment_status' => [ 'closed' | 'open' ] // 'open' ならコメントを許可。デフォルトはオプション 'default_comment_status' の値、または 'closed'。
            //'post_category'  => [ array(<カテゴリー ID>, ...) ] // 投稿カテゴリー。デフォルトは空（カテゴリーなし）。
            //'tags_input'     => [ '<tag>, <tag>, ...' | array ] // 投稿タグ。デフォルトは空（タグなし）。
            //'tax_input'      => [ array( <タクソノミー> => <array | string>, ...) ] // カスタムタクソノミーとターム。デフォルトは空。
            //'page_template'  => [ <文字列> ] // テンプレートファイルの名前、例えば template.php 等。デフォルトは空。
        ];
        
        if( (int)$model->wp_post_id>0 )
        {
            $post['ID'] = $model->wp_post_id;
        }
        if( $model->open_status == 1 )
        {
            $post['post_status'] = 'publish';
        }
        else
        {
            $post['post_status'] = 'draft';
        }
        
        $return = wp_insert_post( $post );
        
        if( $return === 0 )
        {
            return false;
        }
        
        $model->wp_post_id = $return;
        
        return $return;
    }
    
    // ----------------------------------------------------
    /**
     * 画像用リストの作成
     */
    protected function imageList()
    {
        
        
    }
    
    // ----------------------------------------------------
    /**
     * 登録
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductsModel();
        $model->loadDefaultValues();
        
        //var_dump(wp_upload_dir());
        //保存処理
        if ( $model->load(Yii::$app->request->post()) ) {
            
            //初期値設定
            $model->create_at = date('Y-m-d H:i:s');
            $model->update_at = date('Y-m-d H:i:s');
            if( $model->wp_post_id == '' )
            {
                $model->wp_post_id = null;
            }
            
            
            if( $model->validate() )
            {
                //WP処理
                $return = $this->wppost( $model );
                
                //保存
                $model->save();
                return $this->redirect(['index']);
            }
            
        }
        
        
        //関連画像モデル
        $imageModel = new RelProductAttachModel();
        
        
        //SKUモデル
        $skuModel = new ProductSkuModel;
        
        
        //SKU画像モデル
        $skuImageModel = new RelSkuAttachModel;
        
        
        //オプション
        $optionModel = new ProductOptionModel;
        $optionModel->loadDefaultValues();
        $optionAttributesInit = $optionModel->attributes();
        
        //オプション値
        $optionValueModel = new ProductOptionValueModel;
        
        
        //追加テキスト
        $productAddTextModel = new ProductAddTextModel;
        
        
        return $this->render('create', [
            'model' => $model,
            'imageModel' => $imageModel,
            'skuModel' => $skuModel,
            'skuImageModel' => $skuImageModel,
            'optionModel' => $optionModel,
            'optionValueModel' => $optionValueModel,
            'productAddTextModel' => $productAddTextModel,
            'optionAttributesInit' => $optionAttributesInit,
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Updates an existing ProductsModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ProductsModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductsModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductsModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductsModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
