<?php
/* =============================================================================
 * 顧客名定義
 * ========================================================================== */
namespace app\controllers\manage;

use Yii;
use app\models\CustomerFieldDefineModel;
use app\models\CustomerFieldDefineSearchModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Customer_field_defineController implements the CRUD actions for CustomerFieldDefineModel model.
 */
class Customer_field_defineController extends Controller
{
    // ----------------------------------------------------
    /**
     * 振る舞い設定
     * @return type
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    // ----------------------------------------------------
    /**
     * 一覧
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerFieldDefineSearchModel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    // ----------------------------------------------------
    /**
     * 新規作成
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerFieldDefineModel();
        $model->loadDefaultValues();
        
        //データ登録
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            
            
            
            
            
            
            
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        
        
        return $this->render('create', [
                'model' => $model,
            ]);
    }
    
    // ----------------------------------------------------
    /**
     * 更新
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    // ----------------------------------------------------
    /**
     * 削除
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    // ----------------------------------------------------
    
    /**
     * データ特定してロード
     * @param integer $id
     * @return CustomerFieldDefineModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerFieldDefineModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    // ----------------------------------------------------
}
