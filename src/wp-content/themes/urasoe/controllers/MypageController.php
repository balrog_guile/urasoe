<?php
/* =============================================================================
 * マイページ
 * ========================================================================== */
namespace app\controllers;

use Yii;
use app\models\CustomerModel;
use app\models\CustomerSearchModel;
use app\models\WpUsersValidationModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\CustomerDelivListModel;

class MypageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            
            
        ];
    }
    
    // ----------------------------------------------------
    /**
     * ログイン
     */
    public function actionLogin()
    {
        //現在のユーザー情報
        $user = wp_get_current_user();
        if( $user->ID > 0 )
        {
            return $this->redirect(['index']);
        }
        
        $data = [];
        $data['vmodel'] = new WpUsersValidationModel;
        $data['vmodel']->setScenario('register');
        $data['error'] = '';
        if( $data['vmodel']->load(Yii::$app->request->post()) )
        {
            if( $data['vmodel']->validate() )
            {
                $creds = [];
                $creds['user_login'] = $data['vmodel']->user_login;
                $creds['user_password'] = $data['vmodel']->user_pass;
                $creds['remember'] = true;
                $user = wp_signon( $creds, false );
                if ( is_wp_error($user) )
                {
                   $data['error'] = $user->get_error_message();
                }
                else
                {
                    return $this->redirect(['index']);
                }
            }
        }
        
        
        $this->layout = 'wordpress';
        return $this->render( 'login', $data );
    }
    
    // ----------------------------------------------------
    /**
     * サインアップ
     */
    public function actionSignup()
    {
        $data = [];
        $data['model'] = new CustomerModel;
        $data['model']->loadDefaultValues();
        $data['vmodel'] = new WpUsersValidationModel;
        
        $data['mode'] = 'input';
        
        
        if(
            $data['model']->load(Yii::$app->request->post())
            && $data['vmodel']->load(Yii::$app->request->post())
        )
        {
            
            //確認へ移動
            if( $data['vmodel']->valiationMode == 'input' )
            {
                //仮データ
                $data['model']->create_at = date('Y-m-d H:i:s');
                $data['model']->update_at = date('Y-m-d H:i:s');
                $data['model']->user_id = 0;
                
                if(
                    $data['model']->validate()
                    && $data['vmodel']->validate()
                )
                {
                    $data['mode'] = 'confirm';
                }
            }
            
            //再入力
            if( $data['vmodel']->valiationMode == 'reinput' )
            {
                $data['mode'] = 'input';
                $data['vmodel']->valiationMode = 'input';
            }
            
            
            //登録完了
            if( $data['vmodel']->valiationMode == 'exec' )
            {
                //WPのユーザー作成
                $id = wp_create_user(
                    $data['vmodel']->user_login,
                    $data['vmodel']->user_pass,
                    $data['vmodel']->user_email
                );
                
                //role
                $user_id_role = new \WP_User($id);
                $user_id_role->set_role('yii-user');
                
                //データセーブ
                $data['model']->create_at = date('Y-m-d H:i:s');
                $data['model']->update_at = date('Y-m-d H:i:s');
                $data['model']->user_id = $id;
                $data['model']->mailaddress1 = $data['vmodel']->user_email;
                
                if( $data['model']->save() )
                {
                    $this->redirect(['completion']);
                }
                
            }
            
        }
        
        $this->layout = 'wordpress';
        
        return $this->render( 'signup', $data );
    }
    
    // ----------------------------------------------------
    /**
     * 配送先登録編集
     */
    public function actionDeliv_list( $id = null )
    {
        $data = [];
        $this->getCustomerData($data);
        
        if( $id === null )
        {
            $data['model'] = new CustomerDelivListModel;
            $data['model']->loadDefaultValues();
            $data['model']->customer_id = $data['customer']->id;
        }
        else
        {
            $data['model'] = CustomerDelivListModel::find()
                        ->where(['id' => $id ])
                        ->one();
            if( $data['model'] === null )
            {
                $this->redirect(['mypage']);
                return;
            }
        }
        
        
        //追加処理
        if( $data['model']->load(Yii::$app->request->post()) )
        {
            if( $id === null )
            {
                $data['model']->update_at = date('Y-m-d H:i:s');
                $data['model']->create_at = date('Y-m-d H:i:s');
            }
            else{
                $data['model']->update_at = date('Y-m-d H:i:s');
            }
            
            if( $data['model']->save() )
            {
                $this->redirect(['index']);
                return;
            }
            
        }
        
        
        //表示
        $this->layout = 'wordpress';
        return $this->render( 'delivList', $data );
    }
    
    
    // ----------------------------------------------------
    /**
     * 配送先削除
     */
    public function actionDeliv_delete( $id = 0 )
    {
        $data['model'] = CustomerDelivListModel::find()
                    ->where(['id' => $id ])
                    ->one();
        if( $data['model'] === null )
        {
            $this->redirect(['index']);
            return;
        }
        
        $data['model']->delete();
        
        $this->redirect(['index']);
    }
    
    
    // ----------------------------------------------------
    /**
     * ログイン
     */
    public function actionCompletion()
    {
        $data = [];
        $this->layout = 'wordpress';
        return $this->render( 'completion', $data );
    }
    
    // ----------------------------------------------------
    /**
     * ログインデータの取得
     * @param ref $data
     */
    protected function getCustomerData( &$data )
    {
        $data['wpUser'] = wp_get_current_user();
        $data['customer']  = CustomerModel::findOne([ 'user_id' => $data['wpUser']->ID] );
    }
    
    // ----------------------------------------------------
    
    /**
     * Lists all CustomerModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        //ログインデータの取得
        $data = [];
        
        
        //顧客データ取得
        $this->getCustomerData($data);
        
        
        //配送先データ
        $data['deliveryList'] = CustomerDelivListModel::find()
                ->where([ 'customer_id' => $data['customer']->id ])
                ->orderBy('rank')
                ->all();
        
        
        //表示処理
        $this->layout = 'wordpress';
        return $this->render('index', $data );
    }
    
    // ----------------------------------------------------
    
    /**
     * Displays a single CustomerModel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    // ----------------------------------------------------
    
    /**
     * Creates a new CustomerModel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerModel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    // ----------------------------------------------------
    
    /**
     * Updates an existing CustomerModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    // ----------------------------------------------------
    
    /**
     * Deletes an existing CustomerModel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    // ----------------------------------------------------
    
    /**
     * Finds the CustomerModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
