<?php
namespace app\controllers;
use Yii;
use app\helper\WpTemplate;

class WordpressController extends \yii\web\Controller
{
    // ----------------------------------------------------
    /**
     * 初期インデックス
     * @return type
     */
    
    public function actionIndex()
    {
        //ロールチェック
        $this->checkRole();
        
        
        //ルーティングを書き換える
        $route = $this->checkMethod();
        
        
        if( $route !== FALSE )
        {
            $param = [];
            foreach( $_GET as $key => $val )
            {
                $param[$key] = $val;
            }
            return Yii::$app->runAction($route, $param);
        }
        return $this->wp();
    }
    
    // ----------------------------------------------------
    /**
     * yii用のユーザー権限がWPにあるかチェック
     */
    public function checkRole( $force = FALSE )
    {
        $roleName = array(
            'yii-user' => [
                'name' => 'お客様',
                'enable' => [
                    'read' => true,
                ]
            ],
        );
        
        foreach( $roleName as $name => $set )
        {
            $data = get_role( $name );
            if(
                ( is_null($data) )&&
                ( $force === true )
            )
            {
                add_role(
                        $name ,
                        $set['name'],
                        $set['enable']
                );
            }
        }
        
    }
    
    // ----------------------------------------------------
    /**
     * WP用の外出し
     */
    protected function wp()
    {
        
        if ( is_robots() ) {
            do_action('do_robots');
            return;
        }
        else if ( is_feed() ) {
            do_feed();
            return;
        }
        else if ( is_trackback() ) {
            include(ABSPATH . 'wp-trackback.php');
            return;
        }
        
        
        $template = false;
        if     ( is_404()            && $template = WpTemplate::get_404_template()            ) :
        elseif ( is_search()         && $template =  WpTemplate::get_search_template()         ) :
        elseif ( is_front_page()     && $template =  WpTemplate::get_front_page_template()     ) :
        elseif ( is_home()           && $template =  WpTemplate::get_home_template()           ) :
        elseif ( is_post_type_archive() && $template =  WpTemplate::get_post_type_archive_template() ) :
        elseif ( is_tax()            && $template =  WpTemplate::get_taxonomy_template()       ) :
        elseif ( is_attachment()     && $template =  WpTemplate::get_attachment_template()     ) :
                remove_filter('the_content', 'prepend_attachment');
        elseif ( is_single()         && $template =  WpTemplate::get_single_template()         ) :
        elseif ( is_page()           && $template =  WpTemplate::get_page_template()           ) :
        elseif ( is_singular()       && $template =  WpTemplate::get_singular_template()       ) :
        elseif ( is_category()       && $template =  WpTemplate::get_category_template()       ) :
        elseif ( is_tag()            && $template =  WpTemplate::get_tag_template()            ) :
        elseif ( is_author()         && $template =  WpTemplate::get_author_template()         ) :
        elseif ( is_date()           && $template =  WpTemplate::get_date_template()           ) :
        elseif ( is_archive()        && $template =  WpTemplate::get_archive_template()        ) :
        elseif ( is_comments_popup() && $template =  WpTemplate::get_comments_popup_template() ) :
        elseif ( is_paged()          && $template =  WpTemplate::get_paged_template()          ) :
        else :
            $template =  WpTemplate::get_index_template();
        endif;
        $data = [];
        
        if(is_404())
        {
            echo Yii::$app->response->setStatusCode(404);
        }
        
        return $this->renderFile($template , $data );
        
        /*
        if ( is_robots() ) {
            do_action('do_robots');
            return;
        }
        else if ( is_feed() ) {
            do_feed();
            return;
        }
        else if ( is_trackback() ) {
            include(ABSPATH . 'wp-trackback.php');
            return;
        }
        else if ( is_404() ) {
            echo Yii::$app->response->setStatusCode(404);
            return $this->renderPartial('404' );
        }
        else if ( is_search() )
        {
            return $this->renderPartial('search' );
        }
        else if ( is_tax() )
        {
            return $this->renderPartial('taxonomy' );
        }
        else if ( is_home() )
        {
            return $this->renderPartial('index' );
        }
        else if ( is_attachment() )
        {
            //remove_filter('the_content', 'prepend_attachment');
            return $this->renderPartial('attachment' );
        }
        else if ( is_single() )
        {
            return $this->renderPartial('single' );
        }
        else if ( is_page() )
        {
            return $this->renderPartial('page' );
        }
        else if ( is_category() )
        {
            return $this->renderPartial('archive' );
        }
        else if ( is_tag() )
        {
            return $this->renderPartial('archive' );
        }
        else if ( is_author() )
        {
            return $this->renderPartial('author' );
        }
        else if ( is_date() )
        {
            return $this->renderPartial('archive' );
        }
        else if ( is_archive() )
        {
            return $this->renderPartial('archive' );
        }
        else if ( is_comments_popup() )
        {
            return $this->renderPartial('comments_popup' );
        }
        else if ( is_paged() )
        {
            return $this->renderPartial('paged' );
        }
        else
        {
            return $this->renderPartial('index' );
        }
        */
    }
    
    // ----------------------------------------------------
    /**
     * コントローラーメソッドの存在チェック
     */
    private function checkMethod()
    {
        $pathInfo = explode( '/', Yii::$app->request->pathInfo );
        $controllerList = [];
        while( count($pathInfo) > 0 )
        {
            //
            $controllerList[] = array_shift( $pathInfo );
            $method = $pathInfo[0];
            $controllerListSet = $controllerList;
            $contorollerName = array_pop($controllerListSet);
            $contorollerName = ucfirst( $contorollerName ) . 'Controller';
            
            $controllerFile = 
                Yii::getAlias('@app')
                . '/controllers/'
                . (
                    (
                        empty($controllerListSet)
                    )?'':implode( '/', $controllerListSet ) . '/' 
                  )
                . $contorollerName
                . '.php';
            $method = $pathInfo[0];
            if( $method == '')
            {
                $method = 'index';
            }
            //echo $controllerFile . '<br />';
            //echo $contorollerName . '<br />';
            //echo $method . '<hr />';
            
            if(file_exists($controllerFile))
            {
                $space = 'app\\controllers\\' .
                        (
                            (empty($controllerListSet))?
                            '':
                            implode( '\\', $controllerListSet ). '\\'
                        ) . $contorollerName;
                //echo $space . 'dafdsaffa';
                $setMethod = 'action' . ucfirst($method);
                $methodExists = method_exists( $space, $setMethod );
                if( $methodExists === true )
                {
                    $return = implode( '/', $controllerList ) . '/' . $method;
                    return $return;
                }
            }
        }
        return false;
    }
    
    // ----------------------------------------------------

}
