<?php
$wp_db_version = get_option('db_version');
require_once( Yii::getAlias('@app') . '/../../../wp-admin/admin.php' );
require_once( Yii::getAlias('@app') . '/../../../wp-admin/includes/dashboard.php');
wp_dashboard_setup();
wp_enqueue_script( 'dashboard' );
?>