<?php
// WP用

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<?php get_header(); ?>
<?php $this->beginBody() ?>



<?= $content ?>



<?php $this->endBody() ?>
<?php get_footer(); ?>
<?php $this->endPage() ?>
