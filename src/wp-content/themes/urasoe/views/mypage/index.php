<?php
/* =============================================================================
 * マイページ
 * ========================================================================== */
use yii\helpers\Html;
use yii\grid\GridView;
Yii::$app->wpvars->set('title', 'マイページ');
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <article class="post type-post status-publish format-standard hentry">
            <header class="entry-header">
                <h1 class="entry-title">
                    こんにちは<?= $customer->name1; ?><?= $customer->name2; ?>さん
                </h1>
            </header>
            <div class="entry-content">
                
                <h2>各種操作</h2>
                <a href="javascript:void(0);">登録情報確認／編集</a><br />
                <br />
                
                <hr />
                
                <h2>お買い物履歴</h2>
                <table>
                    <thead>
                        <tr>
                            <th>購入日</th>
                            <th>購入商品</th>
                            <th>　</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>2015/01/01</td>
                            <td>あれそれ…</td>
                            <td>
                                <a href="javascript:void(0);">
                                    詳細
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>2015/01/01</td>
                            <td>あれそれ…</td>
                            <td>
                                <a href="javascript:void(0);">
                                    詳細
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
                <hr>
                
                <h2>配送先</h2>
                <table>
                    <thead>
                        <tr>
                            <th>削除</th>
                            <th>お名前</th>
                            <th>住所／電話番号</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach( $deliveryList as $deliv ): ?>
                        <tr>
                            <td>
                                <?= Html::a(
                                        '削除',
                                        ['mypage/deliv_delete','id'=>$deliv->id],
                                        []
                                )?>
                            </td>
                            <td>
                                <?php if( $deliv->company_name != '' ): ?>
                                    <?= $deliv->company_name; ?>(<?= $deliv->company_kana; ?>)<br />
                                <?php endif; ?>
                                <?= $deliv->name1; ?><?= $deliv->name2; ?><br />
                                <?= $deliv->kana1; ?><?= $deliv->kana2; ?><br />
                            </td>
                            <td>
                                <?= $deliv->zip; ?> <?= $deliv->pref; ?><br />
                                <?= $deliv->address1; ?><br />
                                <?= $deliv->address2; ?><br />
                                <?= $deliv->address3; ?><br />
                                <?= $deliv->tel; ?><br />
                            </td>
                            <td>
                                <?= Html::a( '編集', ['deliv_list','id' => $deliv->id ], [] ); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                <?= Html::a( 'お届け先情報追加', ['deliv_list'], [] ); ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                
                
                
            </div>
        </article>
    </main>
</div>
<hr />