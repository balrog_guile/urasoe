<?php
/* =============================================================================
 * お届け先
 * ========================================================================== */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

Yii::$app->wpvars->set('title', '配送先');
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <article class="post type-post status-publish format-standard hentry">
            <header class="entry-header">
                <h1 class="entry-title">
                    配送先登録
                </h1>
            </header>
            <div class="entry-content">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field( $model, 'customer_id' )->hiddenInput()->label(false) ?>
                <?= $form->field( $model, 'company_name' )->textInput(); ?>
                <?= $form->field( $model, 'company_kana' )->textInput(); ?>
                <?= $form->field( $model, 'name1' )->textInput(); ?>
                <?= $form->field( $model, 'name2' )->textInput(); ?>
                <?= $form->field( $model, 'kana1' )->textInput(); ?>
                <?= $form->field( $model, 'kana2' )->textInput(); ?>
                <?= $form->field( $model, 'zip' )->textInput(); ?>
                <?= $form->field( $model, 'pref' )->textInput(); ?>
                <?= $form->field( $model, 'address1' )->textInput(); ?>
                <?= $form->field( $model, 'address2' )->textInput(); ?>
                <?= $form->field( $model, 'address3' )->textInput(); ?>
                <?= $form->field( $model, 'tel' )->textInput(); ?>
                
                <?= Html::submitButton('登録'); ?>
                
                <?php ActiveForm::end(); ?>
            </div>
        </article>
    </main>
</div>
<hr />