<?php
/* =============================================================================
 * サインアップ用
 * ========================================================================== */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php
//タイトル
Yii::$app->wpvars->set('title', 'ログイン');
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <article class="post type-post status-publish format-standard hentry">
            <header class="entry-header"><h1 class="entry-title">ログイン</h1></header>
            <div class="entry-content">
                
                <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($vmodel, 'user_login')->textInput() ?>
                    <?= $form->field($vmodel, 'user_pass')->passwordInput() ?>
                    <?= Html::submitButton('ログイン'); ?>
                <?php ActiveForm::end(); ?>
                
            </div>
        </article>
    </main>
</div>