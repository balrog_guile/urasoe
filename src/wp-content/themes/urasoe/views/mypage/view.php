<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerModel */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-model-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'name1',
            'name2',
            'kana1',
            'kana2',
            'zip',
            'pref',
            'address1',
            'address2',
            'address3',
            'company_name',
            'company_kana',
            'honor',
            'campany_honor',
            'tel1',
            'tel2',
            'tel3',
            'fax1',
            'fax2',
            'mailaddress1',
            'mailaddress2',
            'url1:url',
            'url2:url',
            'create_at',
            'update_at',
            'customer_status',
        ],
    ]) ?>

</div>
