<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'name1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kana1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kana2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_kana')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'honor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'campany_honor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tel1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tel2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tel3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fax1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fax2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mailaddress1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mailaddress2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'create_at')->textInput() ?>

    <?= $form->field($model, 'update_at')->textInput() ?>

    <?= $form->field($model, 'customer_status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
