<?php
/* =============================================================================
 * サインアップ用
 * ========================================================================== */
?>
<?php
//タイトル
Yii::$app->wpvars->set('title', 'サインアップ完了');
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <article class="post type-post status-publish format-standard hentry">
            <header class="entry-header"><h1 class="entry-title">サインアップ完了</h1></header>
            <div class="entry-content">
                サインアップ完了しました。
            </div>
        </article>
    </main>
</div>