<?php
/* =============================================================================
 * サインアップ用
 * ========================================================================== */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php
//タイトル
Yii::$app->wpvars->set('title', 'サインアップ');
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <article class="post type-post status-publish format-standard hentry">
            <header class="entry-header"><h1 class="entry-title">サインアップ</h1></header>
            <div class="entry-content">
                
                
                <!-- 入力 -->
                <?php if( $mode == 'input'): ?>
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($vmodel, 'valiationMode')->hiddenInput()->label(false) ?>
                    <?= $form->field($vmodel, 'user_login')->textInput() ?>
                    <?= $form->field($vmodel, 'user_email')->textInput() ?>
                    <?= $form->field($vmodel, 'user_pass')->passwordInput() ?>
                    
                    <?= $form->field($model, 'name1')->textInput() ?>
                    <?= $form->field($model, 'name2')->textInput() ?>
                    <?= $form->field($model, 'kana1')->textInput() ?>
                    <?= $form->field($model, 'kana2')->textInput() ?>
                    
                    <?= $form->field($model, 'zip')->textInput() ?>
                    <?= $form->field($model, 'pref')->textInput() ?>
                    <?= $form->field($model, 'address1')->textInput() ?>
                    <?= $form->field($model, 'address2')->textInput() ?>
                    <?= $form->field($model, 'address3')->textInput() ?>
                    
                    <?= $form->field($model, 'tel1')->textInput() ?>
                    
                    <hr />
                    
                    <?= Html::submitButton('確認画面へ'); ?>
                    <?php ActiveForm::end(); ?>
                <?php endif; ?>
                <!-- /入力 -->
                
                
                <!-- 確認 -->
                <?php if( $mode == 'confirm'): ?>
                    
                    <?= $vmodel->getAttributeLabel( 'user_login' ); ?>
                    <?= $vmodel->user_login; ?>
                    
                    <br />
                    
                    <?= $vmodel->getAttributeLabel( 'user_email' ); ?>
                    <?= $vmodel->user_email; ?>
                    
                    <br />
                    
                    <?= $vmodel->getAttributeLabel( 'user_pass' ); ?>
                    非表示
                    
                    <br />
                    
                    <label>お名前</label>
                    <?= $model->name1; ?> <?= $model->name2; ?>
                    
                    <br />
                    
                    <label>かな</label>
                    <?= $model->kana1; ?> <?= $model->kana2; ?>
                    
                    <br />
                    
                    <label>ご住所</label>
                    <?= $model->zip; ?><br />
                    <?= $model->pref; ?><br />
                    <?= $model->address1; ?><br />
                    <?= $model->address2; ?><br />
                    <?= $model->address3; ?><br />
                    
                    <br />
                    
                    <label>お電話</label>
                    <?= $model->tel1; ?>
                    
                    <!-- 再入力 -->
                    <?php $form = ActiveForm::begin(); ?>
                        <?php
                            $vmodelAttr = ['user_login','user_email', 'user_pass' ];
                            $modelAttr = [
                                'name1', 'name2', 'kana1', 'kana2', 'zip', 'pref',
                                'address1', 'address2', 'address3', 'tel1'
                            ];
                        ?>
                        <?php foreach( $vmodelAttr as $one ): ?>
                            <?= $form->field($vmodel, $one)->hiddenInput()->label(false) ?>
                        <?php endforeach; ?>
                        <?php foreach( $modelAttr as $one ): ?>
                            <?= $form->field($model, $one)->hiddenInput()->label(false) ?>
                        <?php endforeach; ?>
                        <?= Html::hiddenInput( 'WpUsersValidationModel[valiationMode]', 'reinput' ); ?>
                        <?= Html::submitInput('再入力'); ?>
                    <?php ActiveForm::end(); ?>
                    <!-- /再入力 -->
                    
                    
                    <!-- 実行 -->
                    <?php $form = ActiveForm::begin(); ?>
                        <?php foreach( $vmodelAttr as $one ): ?>
                            <?= $form->field($vmodel, $one)->hiddenInput()->label(false) ?>
                        <?php endforeach; ?>
                        <?php foreach( $modelAttr as $one ): ?>
                            <?= $form->field($model, $one)->hiddenInput()->label(false) ?>
                        <?php endforeach; ?>
                        <?= Html::hiddenInput( 'WpUsersValidationModel[valiationMode]', 'exec' ); ?>
                        <?= Html::submitInput('登録'); ?>
                    <?php ActiveForm::end(); ?>
                    <!-- /実行 -->
                    
                    
                    
                <?php endif; ?>
                <!-- /確認 -->
                
                
                
                
            </div>
        </article>
    </main>
</div>