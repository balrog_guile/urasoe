<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Models');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Customer Model'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'name1',
            'name2',
            'kana1',
            // 'kana2',
            // 'zip',
            // 'pref',
            // 'address1',
            // 'address2',
            // 'address3',
            // 'company_name',
            // 'company_kana',
            // 'honor',
            // 'campany_honor',
            // 'tel1',
            // 'tel2',
            // 'tel3',
            // 'fax1',
            // 'fax2',
            // 'mailaddress1',
            // 'mailaddress2',
            // 'url1:url',
            // 'url2:url',
            // 'create_at',
            // 'update_at',
            // 'customer_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
