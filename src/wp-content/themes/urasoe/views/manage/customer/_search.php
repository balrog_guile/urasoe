<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerSearchModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-model-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'name1') ?>

    <?= $form->field($model, 'name2') ?>

    <?= $form->field($model, 'kana1') ?>

    <?php // echo $form->field($model, 'kana2') ?>

    <?php // echo $form->field($model, 'zip') ?>

    <?php // echo $form->field($model, 'pref') ?>

    <?php // echo $form->field($model, 'address1') ?>

    <?php // echo $form->field($model, 'address2') ?>

    <?php // echo $form->field($model, 'address3') ?>

    <?php // echo $form->field($model, 'company_name') ?>

    <?php // echo $form->field($model, 'company_kana') ?>

    <?php // echo $form->field($model, 'honor') ?>

    <?php // echo $form->field($model, 'campany_honor') ?>

    <?php // echo $form->field($model, 'tel1') ?>

    <?php // echo $form->field($model, 'tel2') ?>

    <?php // echo $form->field($model, 'tel3') ?>

    <?php // echo $form->field($model, 'fax1') ?>

    <?php // echo $form->field($model, 'fax2') ?>

    <?php // echo $form->field($model, 'mailaddress1') ?>

    <?php // echo $form->field($model, 'mailaddress2') ?>

    <?php // echo $form->field($model, 'url1') ?>

    <?php // echo $form->field($model, 'url2') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'customer_status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
