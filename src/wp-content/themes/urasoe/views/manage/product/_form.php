<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helper\UrasoeVar;


/* @var $this yii\web\View */
/* @var $model app\models\ProductsModel */
/* @var $form yii\widgets\ActiveForm */

?>


<!-- asset -->
<script>
var setUrl = {
    'baseUrl': '<?= get_template_directory_uri(); ?>/addassets/admin/',
};
</script>
<?php
use app\addassets\admin\ProductAsset;
ProductAsset::register($this);
?>
<!-- /asset -->


<div class="products-model-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'item_id')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'open_status')->dropDownList( UrasoeVar::openStatus() ); ?>
        </div>
    </div>
    
    <hr />
    <?= $form->field($model, 'meta_key')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'meta_desc')->textarea(['rows' => 6]) ?>
    <hr />
    
    
    <?= $form->field($model, 'view_commente')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'detail_commente')->textarea(['rows' => 6]) ?>
    
    
    <hr />
    
    
    
    <!-- 画像追加 -->
    <h3>画像</h3>
    
    <ul id="image-list" class="list-unstyled" data-bind="foreach: productImage">
        <li>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 uploded-files">
                            <img data-bind="attr: { src:imageUrl }" src="">
                            
                            <!-- 隠しフィールド注意 -->
                            <?= $form->field( $imageModel, 'product_id' )->hiddenInput()->label(false); ?>
                            <?= $form
                                    ->field( $imageModel, 'attache_id' )
                                    ->hiddenInput([
                                        'data-bind'=>'attr:{ value: attache_id }'
                                    ])
                                    ->label(false);
                            ?>
                            <!-- 隠しフィールド注意 -->
                            
                        </div>
                        <div class="col-md-6">
                            <?=
                                $form->field( $imageModel, 'label' )
                                    ->textInput( ['data-bind' => 'textInput:label'] )
                             ?>
                            <hr />
                            <?=
                                $form->field( $imageModel, 'title' )
                                    ->textInput( ['data-bind' => 'textInput:title'] )
                             ?>
                            <hr />
                            <?=
                                $form->field( $imageModel, 'comment' )
                                    ->textarea( ['data-bind' => 'textInput:comment'] )
                             ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form
                                ->field( $imageModel, 'is_main' )
                                ->radioList([
                                    1 => 'はい',
                                    0 => 'いいえ'
                                ])
                            ?>
                            <?= $form
                                ->field( $imageModel, 'is_thumb' )
                                ->radioList([
                                    1 => 'はい',
                                    0 => 'いいえ'
                                ])
                            ?>
                            <hr>
                            <?= Html::button( '▲', [ 'class' => 'btn', 'data-bind' => 'click: function(data, event){ rankChange("up"); }' ] ); ?>
                            <?= Html::button( '▼', [ 'class' => 'btn', 'data-bind' => 'click: function(data, event){ rankChange("down"); }' ] ); ?>
                            <?= Html::button( '削除', [ 'class' => 'btn btn-danger' ] ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    <div id="image-list-droparea" class="margin-bottom-12">
        ここにドラッグアンドドロップ
    </div>
    <?= Html::fileInput( 'file', '', [ 'class' => 'image-add-btn'] ); ?>
    <!-- /画像追加 -->
    
    
    
    <hr />
    
    
    
    <!-- 追加テキスト -->
    <h3>追加テキスト</h3>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>タイトル</th>
                <th>コンテンツ</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody data-bind="foreach:addText">
            <tr>
                <td>
                    <?= $form
                        ->field( $productAddTextModel, '[]title')
                        ->textInput()
                    ?>
                </td>
                <td>
                    <?= $form
                        ->field( $productAddTextModel, '[]content')
                        ->textarea([])
                    ?>
                </td>
                <td>
                    <?= Html::button( '▲', [ 'class' => 'btn btn-xs', 'data-bind' => '' ] ); ?>
                    <?= Html::button( '▼', [ 'class' => 'btn btn-xs', 'data-bind' => '' ] ); ?>
                    <?= Html::button( '削除', [ 'class' => 'btn btn-xs btn-danger' ] ); ?>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3">
                    <?= Html::a( '追加', 'javascript:void(0);', ['class' => 'btn-add-text btn btn-default btn-sm' ] ); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <!-- /追加テキスト -->
    
    
    <hr />
    
    
    <!-- SKU -->
    <h3>SKU</h3>
    <table class="table table-bordered table-striped">
        <tbody  data-bind="foreach: skuList">
            <tr>
                <th>
                    <span data-bind="html: countDisplay"></span>
                </th>
                <td>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field( $skuModel, 'sku_id')->textInput() ?><br />
                            <?= $form->field( $skuModel, 'sku_name')->textInput() ?><br />
                            <?= $form->field( $skuModel, 'comment')->textarea(); ?>
                            <hr />
                            <?= Html::button( '▲', [ 'class' => 'btn', 'data-bind' => 'click: function(data, event){ rankChange("up"); }' ] ); ?>
                            <?= Html::button( '▼', [ 'class' => 'btn', 'data-bind' => 'click: function(data, event){ rankChange("down"); }' ] ); ?>
                            <?= Html::button( '削除', [ 'class' => 'btn btn-danger' ] ); ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field( $skuModel, 'price')->textInput() ?><br />
                            <?= $form->field( $skuModel, 'sale_price')->textInput() ?>
                            <hr />
                            <?= $form->field( $skuModel, 'campaign_price')->textInput() ?><br />
                            <?= $form->field( $skuModel, 'campaign_begin')->textInput() ?><br />
                            <?= $form->field( $skuModel, 'campaign_end')->textInput() ?><br />
                        </div>
                        
                        <!-- SKU画像 -->
                        <div class="col-md-7">
                            <div
                                id="sku-image-list-droparea"
                                class="margin-bottom-12"
                                data-bind="event:{
                                    'dragenter': function(data, event){
                                        dragEnter( data, event, $element );
                                    },
                                    'dragover': function(data, event){
                                        dragover( data, event, $element );
                                    },
                                    'dragleave': function(data, event){
                                        dragLeave( data, event, $element );
                                    },
                                    'drop': function(data, event){
                                        drop( data, event, $element );
                                    }
                                }"
                            >
                                ここにドラッグアンドドロップ
                            </div>
                            <?=
                                Html::fileInput(
                                    'file',
                                    '',
                                    [
                                        'class' => 'sku-image-add-btn',
                                        'data-bind' => 'attr: {"data-sku-index":count},event:{change: function(data, event){ fileChange(data, event, $element); } }'
                                    ]
                                );
                            ?>
                            <ul
                                id="sku-image-list"
                                class="list-unstyled margin-bottom-12"
                                data-bind="foreach: skuImage"
                            >
                                <li>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-3 uploded-files">
                                                    <img data-bind="attr: { src:imageUrl }" src="">
                                                    
                                                    <!-- 隠しフィールド注意 -->
                                                    <!-- 隠しフィールド注意 -->
                                                </div>
                                                <div class="col-md-6">
                                                    <?=
                                                        $form->field( $skuImageModel, 'label' )
                                                            ->textInput( ['data-bind' => 'textInput:label'] )
                                                     ?>
                                                    <hr />
                                                    <?=
                                                        $form->field( $skuImageModel, 'title' )
                                                            ->textInput( ['data-bind' => 'textInput:title'] )
                                                     ?>
                                                    <hr />
                                                    <?=
                                                        $form->field( $skuImageModel, 'comment' )
                                                            ->textarea( ['data-bind' => 'textInput:comment'] )
                                                     ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <?= Html::button( '▲', [ 'class' => 'btn', 'data-bind' => 'click: function(data, event){ rankChange("up"); }' ] ); ?>
                                                    <?= Html::button( '▼', [ 'class' => 'btn', 'data-bind' => 'click: function(data, event){ rankChange("down"); }' ] ); ?>
                                                    <?= Html::button( '削除', [ 'class' => 'btn btn-danger' ] ); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- /SKU画像 -->
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <?= Html::a( '追加', 'javascript:void(0);', ['class' => 'btn-add-sku btn btn-default btn-sm' ] ); ?>
    <!-- /SKU -->
    
    
    <hr />
    
    
    <!-- オプション項目 -->
    <div id="option-values">
        <h3>オプション項目</h3>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>コード／項目名</th>
                    <th>データ・タイプ</th>
                    <th>値</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: productOption">
                <tr>
                    <td>
                        <?= $form->field( $optionModel, 'code' )->textInput(['data-bind' => 'textInput:code']) ?><br />
                        <?= $form->field( $optionModel, 'name' )->textInput(['data-bind' => 'textInput:name']) ?>
                    </td>
                    <td>
                        <?= $form
                                ->field($optionModel, 'data_type' )
                                ->dropDownList(
                                    [
                                        1 => '自由入力',
                                        2 => '自由入力(複数行)',
                                        3 => 'ラジオ',
                                        4 => 'セレクト',
                                        5 => 'チェックボックス'
                                    ],
                                    ['data-bind' => 'textInput:data_type']
                                );
                        ?>
                        <?= $form->field( $optionModel, 'required' )->checkbox([]); ?>
                    </td>
                    <td>
                        <!-- オプション値 -->
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ラベル</th>
                                    <th>値</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: values ">
                                <tr>
                                    <td>
                                        <?= $form
                                            ->field( $optionValueModel, 'label')
                                            ->textInput([])
                                        ?>
                                    </td>
                                    <td>
                                        <?= $form
                                            ->field( $optionValueModel, 'value')
                                            ->textInput()
                                        ?>
                                    </td>
                                        <td>
                                            <?= Html::button( '▲', [ 'class' => 'btn btn-xs', 'data-bind' => '' ] ); ?>
                                            <?= Html::button( '▼', [ 'class' => 'btn btn-xs', 'data-bind' => '' ] ); ?>
                                            <?= Html::button( '削除', [ 'class' => 'btn btn-xs btn-danger' ] ); ?>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">
                                        <?=
                                            Html::button(
                                                '追加',
                                                [
                                                    'class' => 'btn btn-default btn-sm',
                                                    'data-bind' => 'event:{ click: function(data, event){ addOptionValue(data, event, $element); } }'
                                                ]
                                            );
                                        ?>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <!-- /オプション値 -->
                    </td>
                    <td>
                        <?=
                            Html::button(
                                '▲',
                                [
                                    'class' => 'btn btn-xs',
                                    'data-bind' => 'event:{
                                        click: function(data, event){ rankChange(data, event, $element); },
                                    }',
                                    'data-mode'=>'up'
                                ]
                            );
                        ?>
                        <?=
                            Html::button(
                                '▼',
                                [
                                    'class' => 'btn btn-xs',
                                    'data-bind' => 'event:{
                                        click: function(data, event){ rankChange(data, event, $element); },
                                    }',
                                    'data-mode'=>'down'
                                ]
                            );
                        ?>
                        <?=
                            Html::button(
                                '削除dd',
                                [
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-bind' => 'event:{
                                        click: function(data, event){ lineDelete(data, event, $element); },
                                    }',
                                    'data-mode'=>'down'
                                ]
                            );
                        ?>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5">
                        <?= Html::a( '追加', 'javascript:void(0);', ['class' => 'btn-add-option btn btn-default btn-sm' ] ); ?>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /オプション項目 -->
    
    
    
    
    <!-- 隠し項目 -->
    <?= $form->field($model, 'wp_post_id')->hiddenInput()->label(false) ?>
    <!-- /隠し項目 -->
    
    
    <hr />
    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', '作成') : Yii::t('app', '編集'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
</div>

<!-- json出力 -->
<script>

//テーブルスキーム
var tableSchema = {
    'productOption': <?= json_encode( $optionAttributesInit ) ?>,
};


</script>
<!-- /json出力 -->