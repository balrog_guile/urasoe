<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductsModel */

$this->title = Yii::t('app', '製品登録');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-model-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <hr />
    
    <?= $this->render('_form', [
        'model' => $model,
        'imageModel' => $imageModel,
        'skuModel' => $skuModel,
        'skuImageModel' => $skuImageModel,
        'optionModel' => $optionModel,
        'optionValueModel' => $optionValueModel,
        'productAddTextModel' => $productAddTextModel,
        'optionAttributesInit' => $optionAttributesInit,
    ]) ?>

</div>
