<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '製品管理');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <hr />
    
    <p>
        <?= Html::a(Yii::t('app', '新規登録'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <hr />
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'wp_post_id',
            'item_id',
            'item_name',
            'url:url',
            // 'open_status',
            // 'meta_key',
            // 'meta_desc:ntext',
            // 'view_commente:ntext',
            // 'detail_commente:ntext',
            // 'create_at',
            // 'update_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
