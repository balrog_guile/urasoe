<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FilemanagerModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="filemanager-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wp_file_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wp_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'wp_attach_id')->textInput() ?>

    <?= $form->field($model, 'create_at')->textInput() ?>

    <?= $form->field($model, 'update_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
