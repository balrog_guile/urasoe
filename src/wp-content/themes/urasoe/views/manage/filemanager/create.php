<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FilemanagerModel */

$this->title = Yii::t('app', 'Create Filemanager Model');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Filemanager Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filemanager-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
