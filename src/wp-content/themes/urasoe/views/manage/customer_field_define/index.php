<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerFieldDefineSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', '顧客フィールド定義');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-field-define-model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <hr />
    
    <p>
        <?= Html::a(Yii::t('app', '新規フィールド'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <hr />
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'data_name',
            'label',
            'data_type',
            'create_at',
            // 'update_at',
            // 'rank',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
