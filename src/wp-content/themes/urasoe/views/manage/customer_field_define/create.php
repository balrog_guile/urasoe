<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerFieldDefineModel */

$this->title = Yii::t('app', '新規フィールド');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', '顧客フィールド定義'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-field-define-model-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <hr />
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
