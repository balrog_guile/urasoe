<?php
/* =============================================================================
 * WPのroleをチェック
 * ========================================================================== */
namespace app\filters;
use Yii;
use yii\filters\AccessRule;

/****
 * Roleの名前
 * 
 * @:すべて
 * &:管理権限
 * 
 */



class AccessRule2 extends AccessRule
{

    /** @inheritdoc */
    protected function matchRole($user)
    {
        if( in_array( '*', $this->roles ) )
        {
            return true;
        }
        if (empty($this->roles)) {
            return true;
        }
        
        
        //ユーザー情報
        $user = wp_get_current_user();
        
        
        //ログインなしならfalse
        if( $user->ID == 0 )
        {
            return false;
        }
        
        
        $caps = array_keys( $user->caps );
        
        if( in_array('yii-user', $caps ) )
        {
            $wpUserRank = 0;
        }
        else
        {
            $wpUserRank = 1;
        }
        
        foreach ($this->roles as $role)
        {
            if( $role == '@' )
            {
                return true;
            }
            if( ( $role == '&' )&&( $wpUserRank == 1 ) )
            {
                return true;
            }
        }
        return false;
    }
}