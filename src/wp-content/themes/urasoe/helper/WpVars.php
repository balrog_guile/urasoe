<?php
/* =============================================================================
 * WPテンプレートに対してデータを共有するためのヘルパ
 * ========================================================================== */
namespace app\helper;
class WpVars
{
    //データ保存用
    public $dataList = [];
    
    // ----------------------------------------------------
    /**
     * コンストラクタ
     */
    public function __construct()
    {
        $this->init();
    }
    
    // ----------------------------------------------------
    /**
     * データ初期化
     */
    public function init()
    {
        $this->dataList = [];
    }
    // ----------------------------------------------------
    /**
     * データ設定
     * @param string $name
     * @param mix $data
     */
    public function set( $name, $data )
    {
        $this->dataList[$name] = $data;
    }
    
    // ----------------------------------------------------
    /**
     * データ取得
     * @param string $name
     */
    public function get( $name )
    {
        if(isset($this->dataList[$name] ) )
        {
            return $this->dataList[$name];
        }
        return false;
    }
    
    // ----------------------------------------------------
    /**
     * データクリア
     */
    public function clear( $name )
    {
        if(!isset($this->dataList[$name] ) )
        {
            return false;
        }
        unset( $this->dataList[$name] );
        return true;
    }
    
    // ----------------------------------------------------
    /**
     * セッター
     */
    public function __set($name, $value) {
        $this->set($name, $value );
    }
    
    // ----------------------------------------------------
    /**
     * ゲッター
     */
    public function __get($name) {
        $this->{$name} = $this->get($name);
    }
    // ----------------------------------------------------
}