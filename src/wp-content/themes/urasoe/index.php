<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/vendor/autoload.php');
require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/config/web.php');

//wordpressのマジッククォーツを回避
$_GET = stripslashes_deep($_GET);
$_POST = stripslashes_deep($_POST);
$_SESSION = stripslashes_deep($_SESSION);
$_REQUEST = stripslashes_deep($_REQUEST);
$_COOKIE = stripslashes_deep($_COOKIE);

(new yii\web\Application($config))->run();
